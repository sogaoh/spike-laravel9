PHONY:

# Options
SEED   := --seed
NO_DEV :=
FORCE  :=

help:
	cat Makefile

# environments
chmod:
	docker compose exec lighthouse chmod -R a+w storage
	docker compose exec lighthouse chmod -R a+w bootstrap/cache
re-bld:
	docker compose build --no-cache --force-rm

# up/down
upmod:
	@make up
	@make chmod
up:
	docker compose up 
upd:
	docker compose up -d
upb:
	docker compose up -d --build
down:
	docker compose down --remove-orphans
prune:
	docker system prune --volumes
restart:
	@make down
	@make up

# laravel
compi:
	composer install $(NO_DEV)
vendor: composer.json composer.lock
	composer self-update
	composer validate
	composer install ${NO_DEV}
dump:
	composer dump-autoload
clear:
	composer clear-cache
	php artisan view:clear
	php artisan route:clear
	php artisan clear-compiled
	php artisan config:cache
	php artisan cache:clear
	php artisan lighthouse:cache
clr: vendor clear dump

key:
	php artisan key:generate
migrate:
	php artisan migrate $(FORCE)
passport:
	php artisan passport:install
storage:
	php artisan storage:link
seed:
	php artisan create:seed
fresh:
	php artisan migrate:fresh $(SEED) $(FORCE)

version:
	php artisan --version


# exec
back:
	docker compose exec lighthouse bash
db:
	docker compose exec mysql bash